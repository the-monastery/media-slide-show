package com.indusblue.media.video.events {
	
	import flash.events.Event;

	public class VideoControlEvent extends Event {
		
		
		public static const PAUSE:String = "pause";
		public static const PLAY:String = "play";
		
		
		
		public function VideoControlEvent(type:String, bubbles:Boolean=false, cancelable:Boolean=false) {
			
			super(type, bubbles, cancelable);
			
		}
		
		
		
		override public function clone():Event {
			
			return new VideoControlEvent( type, bubbles, cancelable );
			
		}
		
		
	}
}