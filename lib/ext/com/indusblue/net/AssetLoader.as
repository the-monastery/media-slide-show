package com.indusblue.net {

	import flash.display.*;
	import flash.events.*;
	import flash.net.URLRequest;
	
	/**
	 * A movieclip library asset is played back based on totalFrames and bytesLoaded 
	 * onComplete, loaded content is passed back through a callback function 
	 * This class cleans up after itself when loading is complete
	 * 
	 * @author nhillier 30/04/2009
	 * 
	 */
	public class AssetLoader {
	
	
		private var _loader:Loader;
		private var _url:URLRequest;
		private var _readyFrame:int;
		private var _callBack:Function;
		private var _progressMeter:MovieClip;
		private var _errorCall:Function;
		
		
		public function set errorCall( call:Function ):void {
			
			_errorCall = call;
			 
		}
		
		
		/**
		 * 
		 * @param url the location of the content to be loaded in
		 * @param callback callback a function expecting loaded content
		 * @param view a preloader MovieClip asset. Load progress is measured as a percentage of total frames
		 * 
		 */
		public function AssetLoader( url:String, callback:Function, progressMeter:MovieClip =  null ) {
		    
		    _progressMeter = progressMeter;
		    
		    if(_progressMeter) {
		    	_progressMeter.stop();
		    }
		    
		    _url = new URLRequest( url );
					
			_callBack = callback;
		    
		    _loader = new Loader();
			_loader.contentLoaderInfo.addEventListener( ProgressEvent.PROGRESS, onProgress );
			_loader.contentLoaderInfo.addEventListener( Event.COMPLETE, onComplete );
			_loader.contentLoaderInfo.addEventListener( IOErrorEvent.IO_ERROR, onError );
			_loader.load( _url );
		                        
		}
		
		
		
		/**
		 * 
		 * @param parentWidth
		 * @param parentHeight
		 * 
		 */
		public function positionToCenter( parentWidth:Number, parentHeight:Number ):void {
			
			if( _progressMeter != null ) {
				_progressMeter.x = ( _progressMeter.width - parentWidth ) * 0.5;
				_progressMeter.y = ( _progressMeter.height - parentHeight ) * 0.5;
			}
		
		}
		
		
		private function onProgress( e:ProgressEvent ):void {
		    
		    if( _progressMeter != null ) { 
		    	var percent:Number = e.bytesTotal != 0 ? e.bytesLoaded / e.bytesTotal : 0;
				var frame:int = Math.max( 1, Math.ceil( percent * _progressMeter.totalFrames ) );  
				
				_progressMeter.gotoAndStop( frame );	
		    }
		    
		}
		
		
		
		private function onComplete( e:Event ):void {
		    
		    _callBack( _loader.content );
			cleanUp();
			
		}
		
		
		
		private function onError( e:IOErrorEvent ):void {
			
			if( _errorCall != null ) {
				
				_errorCall( e );
				
			}
			else {
				
				trace( e.text );
				
			}
			
			cleanUp();
			
		}
		
		
		private function cleanUp():void {
			
			_loader.contentLoaderInfo.removeEventListener( ProgressEvent.PROGRESS, onProgress );
			_loader.contentLoaderInfo.removeEventListener( Event.COMPLETE, onComplete );
			_loader.contentLoaderInfo.removeEventListener( IOErrorEvent.IO_ERROR, onError );
			_progressMeter = null;
			_loader = null;
			null;
			
		}
	            
	}
}
