package com.indusblue.ui {
	
	import caurina.transitions.Equations;
	import caurina.transitions.Tweener;
	import caurina.transitions.properties.DisplayShortcuts;
	
	import flash.display.MovieClip;
	import flash.events.MouseEvent;
	
	
	/**
	 * A non-display class that composits interactive button functionality to a MovieClip
	 *   
	 * @author ghostmonk
	 * 
	 */
	public class MovieClipButton {
		
		
		
		private var _view:MovieClip;
		private var _clickCallBack:Function;
		
		
		
		/**
		 * retrieve instance of target MovieClip passed to the constructor
		 * 
		 * @return 
		 * 
		 */
		public function get view():MovieClip {
			
			return _view;
			
		}
		
		
		
		/**
		 * 
		 * @param view object used for user interaction
		 * @param clickCallBack function called when view is clicked, clone of MouseEvent.CLICK is passed to the constructor
		 * 
		 */
		public function MovieClipButton( view:MovieClip, clickCallBack:Function ) {
			
			DisplayShortcuts.init();
			
			_view = view;
			_view.stop();
			_view.mouseChildren = false;
			
			_clickCallBack = clickCallBack;
			
			enable();
			
		}
		
		
		
		/**
		 * Turns the view into an interactive button
		 * 
		 * @param animate - set to false is you want the clip to jump to the first frame, 
		 * true if you want it to tween
		 * 
		 */
		public function enable( animate:Boolean = true ):void {
			
			_view.buttonMode = true;
			_view.addEventListener( MouseEvent.CLICK, onClick );
			_view.addEventListener( MouseEvent.ROLL_OVER, onRollOver );
			_view.addEventListener( MouseEvent.ROLL_OUT, onRollOut );
			
			if( animate ){
				onRollOut( null );
			} 
			else {
				_view.gotoAndStop( 1 );
			}
			
			testMouse();
			
		}
		
		
		
		/**
		 * Removes interactivity from the view
		 * 
		 * @param makeActive -  set to true if you want the clip to be disabled with an active
		 * state... active state is the final frame of clip
		 * 
		 */
		public function disable( makeActive:Boolean = false, animate:Boolean = true ):void {
			
			_view.buttonMode = false;
			_view.removeEventListener( MouseEvent.CLICK, onClick );
			_view.removeEventListener( MouseEvent.ROLL_OVER, onRollOver );
			_view.removeEventListener( MouseEvent.ROLL_OUT, onRollOut );
			
			if( makeActive ) {
				if( animate ) onRollOver( null );
				else _view.gotoAndStop( _view.totalFrames );
			} 
			else {
				_view.gotoAndStop( 1 );
			}
			
		}
		
		
		
		private function onClick( e:MouseEvent ):void {
		
			_clickCallBack( e );
			
		}
		
		
		
		private function onRollOver( e:MouseEvent ):void {
		
			var frameRate:int = _view.stage != null ? _view.stage.frameRate : 31;
			Tweener.addTween(
				_view, {
					_frame:_view.totalFrames, 
					time:( _view.totalFrames - _view.currentFrame ) / frameRate,
					transition:Equations.easeNone 
				}
			);
			
		}



		private function onRollOut( e:MouseEvent ):void {
			
			if( _view.stage ) {
				Tweener.addTween(
					_view, {
						_frame:1, 
						time: _view.currentFrame / _view.stage.frameRate,
						transition:Equations.easeNone 
					}
				);
			}
			else {
				_view.gotoAndStop( 1 );
			}
			
		}
		
		
		private function testMouse():void {
			
			//ensure clip is on the stage
			if( _view.stage ) {
				
				if( _view.hitTestPoint( _view.parent.mouseX, _view.parent.mouseY ) ) {
					onRollOver( null );
				}
					
			}
			
		}
		
		
	}
}