package {
	
	import com.indusblue.net.AssetLoader;
	
	import flash.display.Sprite;
	
	[SWF(width=940, height=812, pageTitle="BudCamp MediaPlayer", frameRate=31, backgroundColor=0xC8242B)]
	
	/**
	 * 
	 * @author ghostmonk 09/05/2009
	 * 
	 */
	public class mediaplayer_loader extends Sprite {
		
		private var _loadmeter:PreloaderAsset;
		
		
		public function mediaplayer_loader() {
			
			var appSrc:String = loaderInfo.parameters._path 
				? loaderInfo.parameters._path + "mediaplayer.swf" 
				: "mediaplayer.swf";
			
			_loadmeter = new PreloaderAsset();
			_loadmeter.x = ( stage.stageWidth - _loadmeter.width ) * 0.5;
			_loadmeter.y = ( stage.stageHeight - _loadmeter.height ) * 0.5;
			stage.addChild( _loadmeter );
			new AssetLoader( appSrc, onComplete, _loadmeter );
			
		}
		
		
		
		private function onComplete( app:Sprite ):void {
			
			var xmlSrc:String = loaderInfo.parameters._xmlpath || "mediaplayer_en.xml";
			var path:String = loaderInfo.parameters._path || "";
			var lang:String = loaderInfo.parameters.lang || "en";
			
			stage.removeChild( _loadmeter );
			_loadmeter = null;
			addChild( app );
			
			Object( app ).init( xmlSrc, path, lang );
			
		}
		
		
		
	}
}