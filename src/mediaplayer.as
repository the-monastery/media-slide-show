package {

	import ca.budcamp.mediaSlideshow.ApplicationView;
	import ca.budcamp.mediaSlideshow.navigation.TabNavigator;
	import ca.budcamp.mediaSlideshow.navigation.YearChooser;
	import ca.budcamp.mediaSlideshow.photos.PhotoGallery;
	import ca.budcamp.mediaSlideshow.section.SectionManager;
	import ca.budcamp.mediaSlideshow.section.SectionTitle;
	import ca.budcamp.mediaSlideshow.section.ViewContainer;
	import ca.budcamp.mediaSlideshow.thumbnails.Stepper;
	import ca.budcamp.mediaSlideshow.thumbnails.Thumb;
	import ca.budcamp.mediaSlideshow.thumbnails.ThumbCarousel;
	import ca.budcamp.mediaSlideshow.thumbnails.ThumbCollection;
	import ca.budcamp.mediaSlideshow.video.VideoControlPanel;
	import ca.budcamp.mediaSlideshow.video.VideoPlayer;
	
	import com.indusblue.media.video.CoreVideo;
	import com.indusblue.media.video.ui.PlayPauseToggle;
	import com.indusblue.media.video.ui.SimpleVideoScrubber;
	import com.indusblue.net.XMLLoader;
	
	import flash.display.Sprite;
	
	[SWF(width=940, height=812, pageTitle="BudCamp MediaPlayer", frameRate=31, backgroundColor=0xC8242B)]
	
	/**
	 * All of the components, objects, etc. are created here.
	 * <p>Most of the dependencies in the entire project can be acertained from the construction of this class.</p>
	 * @author ghostmonk 09/05/2009
	 * 
	 */
	public class mediaplayer extends Sprite {
		
		public static var LANG:String;
		
		private var _path:String;
		private var _videoPlayerID:String;
		private var _photoGalleryID:String;
		
		private var _startYear:String;
		private var _startSection:String;
		
		
		
		public function mediaplayer() {}
		
		
		/**
		 * Call this function to initialize the application
		 * 
		 * @param xmlSrc location of xml file
		 * @param path the path to prepend to all loaded assets (except flv for some cockamamie reason)
		 * @param lang en or fr
		 * 
		 */
		public function init( xmlSrc:String, path:String, lang:String ):void {
			
			_path = path;
			LANG = lang;
			
			new XMLLoader( xmlSrc, onXMLLoaded );
			
		}
		
		
		private function onXMLLoaded( xml:XML ):void {
			
			setStartIDs( xml );
			
			_videoPlayerID = xml.videos.@id;
			_photoGalleryID = xml.photos.@id;
			
			var appView:ApplicationView = new ApplicationView( _videoPlayerID, _photoGalleryID );
			var contentView:ViewContainer = new ViewContainer( appView.viewContainer );
			
			var photoGallery:PhotoGallery = createPhotoGallery( appView, xml.photos.year );
			var videoPlayer:VideoPlayer = createVideoPlayer( contentView, xml.videos.year );
			
			var tabNavigation:TabNavigator = new TabNavigator();
			var stepper:Stepper = new Stepper( appView.stepperNext, appView.stepperBack, stage ); 
			
			var yearChooser:YearChooser = new YearChooser( appView.watermark, appView.yearChooserMarker );
			createYearChoices( xml, appView, yearChooser );
			
			var sectionManager:SectionManager = new SectionManager( 
				tabNavigation, 
				new ThumbCarousel( appView.thumbCarousel, stepper ),
				contentView,
				new SectionTitle( appView.sectionTitle, _photoGalleryID ),
				yearChooser
			);
			
			sectionManager.addSection( videoPlayer, appView.videosTab );
			sectionManager.addSection( photoGallery, appView.photosTab );
			
			sectionManager.init( _startSection );
			
			addChild( appView );
			
		}
		
		
		
		private function setStartIDs( xml:XML ):void {
			
			var startSectionXML:XML;
			
			for each( var xItem:XML in xml..year ) {
				startSectionXML = xItem;
				if( xItem.@start == true ) {
					break;
				}
			}
			
			_startYear = startSectionXML.@id;
			_startSection = startSectionXML.parent().@id;
			
		}
		
		
		
		private function createPhotoGallery( appView:ApplicationView, yearList:XMLList ):PhotoGallery {
			
			var gallery:PhotoGallery = new PhotoGallery( 
				_photoGalleryID,
				_startYear
			);
			
			for each( var photos:XML in yearList ) {
				gallery.addCollection( photos.@id, createThumbs( photos.photo, _path ) );
			}
			
			return gallery;
			
		}
		
		
		
		private function createVideoPlayer( contentView:ViewContainer, yearList:XMLList ):VideoPlayer {
			
			var playToggle:PlayPauseToggle = new PlayPauseToggle( 
				contentView.controlPanel,
				contentView.playBtn, 
				contentView.pauseBtn
			);
			
			var scrubber:SimpleVideoScrubber = new SimpleVideoScrubber(
				contentView.scrubAsset,
				contentView.scrubAsset.playbackMeter,
				contentView.scrubAsset.loadProgressBar
			);
			
			var videoPlayer:VideoPlayer = new VideoPlayer( 
				_videoPlayerID,
				new CoreVideo( true ),
				new VideoControlPanel( contentView.controlPanel, playToggle, scrubber ),
				contentView.view,
				_startYear
			);
			
			for each( var videos:XML in yearList ) {
				videoPlayer.addCollection( videos.@id, createThumbs( videos.video, "" ) );
			}
			
			return videoPlayer;
			
		}
		
		
		//@fullAssetPath flvs are loaded in relative to the swf... ANNOYING
		// This variable removes the prepend from flvs assets
		private function createThumbs( list:XMLList, fullAssetPath:String ):ThumbCollection {
			
			var thumbs:ThumbCollection = new ThumbCollection();
			
			for each( var xml:XML in list ) {
				thumbs.push( new Thumb( _path + xml.@thumbSrc, fullAssetPath + xml.@src, xml.@label ) );
			}
			
			return thumbs; 
			
		}		
		
		
		
		private function createYearChoices( xml:XML, appView:ApplicationView, yearChooser:YearChooser ):void {
			
			var padding:int = 10;
			var years:Array = getUniqueElements( xml..year.@id );
			
			for( var i:int = 0; i < years.length; i++ ) {
				var yearBtn:YearChooserAsset = new YearChooserAsset();
				yearBtn.label.field.text = 
				yearBtn.labelMultiply.field.text = years[ i ];
				
				yearChooser.addChooser( years[ i ], yearBtn );
				
				yearBtn.x = ( yearBtn.width + padding ) * i + appView.yearChooserMarker.x; 
				yearBtn.y = appView.yearChooserMarker.y;
				appView.addChild( yearBtn );
			}
			
			yearChooser.currentYear = _startYear;
			
		}
		
		
		
		private function getUniqueElements( xmlList:XMLList ):Array {
			
			var output:Array = [];
			
			for each( var element:String in xmlList ) {
				
				if( output.indexOf( element ) == -1 ) {
					output.push( element );
				}
				
			}
			
			return output;
			
		}
		
		
		
	}
}
