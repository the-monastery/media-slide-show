package ca.budcamp.mediaSlideshow.events {
	
	import flash.events.Event;

	/**
	 * 
	 * @author ghostmonk 13/05/2009
	 * 
	 */
	public class YearChoiceEvent extends Event {
		
		
		
		public static const YEAR_CHOICE:String = "yearChoice";
		
		private var _id:String;
		
		
		
		/**
		 * 
		 * @return 
		 * 
		 */
		public function get id():String {
			
			return _id;
			
		}
		
		
		
		/**
		 * 
		 * @param type
		 * @param id
		 * @param bubbles
		 * @param cancelable
		 * 
		 */
		public function YearChoiceEvent(type:String, id:String, bubbles:Boolean=false, cancelable:Boolean=false) {
			
			_id = id;
			
			super(type, bubbles, cancelable);
			
		}
		
		
		
		override public function clone():Event {
			
			return new YearChoiceEvent( type, id, bubbles, cancelable );
			
		}
		
		
		
	}
}