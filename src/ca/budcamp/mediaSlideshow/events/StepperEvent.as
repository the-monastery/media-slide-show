package ca.budcamp.mediaSlideshow.events {
	
	import flash.events.Event;

	/**
	 * 
	 * @author ghostmonk 13/05/2009
	 * 
	 */
	public class StepperEvent extends Event {
		
		
		
		public static const STEP:String = "step";
		
		private var _isNext:Boolean;
		
		
		
		/**
		 * 
		 * @return 
		 * 
		 */
		public function get isNext():Boolean {
			
			return _isNext;
			
		}
		
		
		
		/**
		 * 
		 * @param type
		 * @param isNext
		 * @param bubbles
		 * @param cancelable
		 * 
		 */
		public function StepperEvent(type:String, isNext:Boolean, bubbles:Boolean=false, cancelable:Boolean=false) {
			
			_isNext = isNext;
			
			super(type, bubbles, cancelable);
			
		}
		
		
		
		override public function clone():Event {
			
			return new StepperEvent( type, isNext, bubbles, cancelable );
			
		}
		
		
		
	}
}