package ca.budcamp.mediaSlideshow.events {
	
	import flash.events.Event;

	/**
	 * 
	 * @author ghostmonk 13/05/2009
	 * 
	 */
	public class ThumbClickEvent extends Event {
		
		
		
		public static const THUMB_CLICK:String = "thumbClick";
		
		private var _id:String;
		private var _index:int;
		
		
		
		/**
		 * 
		 * @return 
		 * 
		 */
		public function get id():String {
			
			return _id;
			
		}
		
		
		
		/**
		 * 
		 * @return 
		 * 
		 */
		public function get index():int {
			
			return _index;
			
		}
		
		
		
		/**
		 * 
		 * @param type
		 * @param fullAsset
		 * @param bubbles
		 * @param cancelable
		 * 
		 */
		public function ThumbClickEvent( type:String, id:String, index:int, bubbles:Boolean=false, cancelable:Boolean=false ) {
			
			_id = id;
			_index = index;
			super(type, bubbles, cancelable);
			
		}
		
		
		
		override public function clone():Event {
			
			return new ThumbClickEvent( type, id, index, bubbles, cancelable );
			
		}
		
		
		
	}
}