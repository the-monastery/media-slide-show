package ca.budcamp.mediaSlideshow.events {
	
	import flash.events.Event;

	/**
	 * 
	 * @author ghostmonk 09/05/2009
	 * 
	 */
	public class SectionNavigationEvent extends Event {
		
		
		
		public static const SECTION_NAVIGATION:String = "sectionNavigation";
		
		private var _id:String;
		
		
		
		/**
		 * 
		 * @return 
		 * 
		 */
		public function get id():String {
			
			return _id;
			
		}
		
		
		
		/**
		 * 
		 * @param type
		 * @param id
		 * @param bubbles
		 * @param cancelable
		 * 
		 */
		public function SectionNavigationEvent(type:String, id:String, bubbles:Boolean=false, cancelable:Boolean=false) {
			
			_id = id;
			
			super(type, bubbles, cancelable);
			
		}
		
		
		
		override public function clone():Event {
			
			return new SectionNavigationEvent( type, id, bubbles, cancelable );
			
		}
		
		
		
	}
}