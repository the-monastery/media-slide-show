package ca.budcamp.mediaSlideshow.thumbnails {
	
	import ca.budcamp.mediaSlideshow.events.ThumbClickEvent;
	
	import caurina.transitions.Equations;
	import caurina.transitions.Tweener;
	
	import com.indusblue.net.AssetLoader;
	import com.indusblue.ui.MovieClipButton;
	
	import flash.display.Bitmap;
	import flash.events.MouseEvent;
	
	[Event (name="thumbClick", type="ca.budcamp.mediaSlideshow.events.ThumbClickEvent")]
	
	public class Thumb extends ThumbAsset {
		
		
		
		private const LABELESS_Y:int = 0;
		
		private var _loader:AssetLoader;
		private var _fullAsset:String;
		private var _button:MovieClipButton;
		private var _isSelected:Boolean;
		private var _index:int;
		
		
		
		public function get index():int {
			return _index;
		}
		
		
		public function set index( value:int ):void {
			_index = value;
		}
		
		
		
		public function get fullAsset():String {
			
			return _fullAsset;
			
		}
		
		
		
		public function get isSelected():Boolean {
			
			return _isSelected;
			
		}
		
		
		
		public function Thumb( url:String, full:String, label:String = "" ) {
			
			_button = new MovieClipButton( this, onClick );
			
			_loader = new AssetLoader( url, onThumbLoaded );
			_fullAsset = full;
			alpha = 0;
			holder.alpha = 0;
			field.text = label;
			
			if( label == "" ) {
				y = LABELESS_Y; 
			}
			
			_isSelected = false;
			
		}
		
		
		
		public function select():void {
			
			_button.disable( true );
			_isSelected = true;
			
		}
		
		
		
		public function deselect():void {
			
			_button.enable();
			_isSelected = false;
			
		}
		
		
		
		public function enable():void {
			
			_button.enable();
			
		}
		
		
		
		public function buildIn():void {
			
			_button.enable();
			Tweener.addTween( this, { time:0.3, alpha:1, transition:Equations.easeNone } );
			
		}
		
		
		
		public function buildOut():void {
			
			_button.disable();
			Tweener.addTween( this, { time:0.3, alpha:0, transition:Equations.easeNone } );
			
		}
		
		
		
		private function onThumbLoaded( bitmap:Bitmap ):void {
			
			holder.addChild( bitmap );
			Tweener.addTween( holder, { alpha:1, time:0.3, transition:Equations.easeNone } ); 
			
		}
		
		
		
		private function onClick( e:MouseEvent ):void {
			
			dispatchEvent( new ThumbClickEvent( ThumbClickEvent.THUMB_CLICK, _fullAsset, index ) );
				
		}
		
		
		
	}
}