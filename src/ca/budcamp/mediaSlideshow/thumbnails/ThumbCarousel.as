package ca.budcamp.mediaSlideshow.thumbnails {
	
	import ca.budcamp.mediaSlideshow.events.StepperEvent;
	
	import caurina.transitions.Tweener;
	
	import com.indusblue.ui.MovieClipButton;
	
	import flash.events.EventDispatcher;
	import flash.events.MouseEvent;
	
	
	[Event (name="step", type="ca.budcamp.mediaSlideshow.events.StepperEvent")]
	
	/**
	 * 
	 * @author ghostmonk 09/05/2009
	 * 
	 */
	public class ThumbCarousel extends EventDispatcher {
		
		
		
		private const THUMB_PADDING:int = 20;
		
		private var _view:ThumbCarouselAsset;
		private var _currentCollection:ThumbCollection; 
		private var _leftScroller:MovieClipButton;
		private var _rightScroller:MovieClipButton;
		private var _stepper:Stepper;
		
		private var _destinationIndices:Array;
		private var _animThumbsAmt:int;
		
		private var _leftMostIndex:int;
		private var _holderWidth:int;
		
		private var _isScrollable:Boolean;
		
		
		
		public function get isScrollable():Boolean {
			
			return _isScrollable;
			
		}
		
		
		
		public function get visibleSpaces():int {
			
			return _animThumbsAmt - 1;
			
		}
		
		
		
		public function set leftMostIndex( value:int ):void {
			
			_leftMostIndex = value;
			
		}
		
		
		
		public function get leftMostIndex():int {
			
			return _leftMostIndex;
			
		}
		
		
		
		public function ThumbCarousel( view:ThumbCarouselAsset, stepper:Stepper ) {
			
			_view = view;
			
			_stepper = stepper;
			_stepper.addEventListener( StepperEvent.STEP, onStep );
			
			_leftScroller = new MovieClipButton( _view.scrollLeft, scrollRight );
			_rightScroller = new MovieClipButton( _view.scrollRight, scrollLeft );
			
			_holderWidth = _view.holder.width;
			
			_isScrollable = false;
			
		}
		
		
		
		public function enable():void {
			
			_leftScroller.enable();
			_rightScroller.enable();
			_leftScroller.view.visible = true;
			_rightScroller.view.visible = true;
			
			_isScrollable = true;
			
		}
		
		
		
		public function disable():void {
			
			_leftScroller.disable();
			_rightScroller.disable();
			_leftScroller.view.visible = false;
			_rightScroller.view.visible = false;
			
			_isScrollable = false;
			
		}
		
		
		
		public function addThumbs( collection:ThumbCollection ):void {
			
			if( _currentCollection != null ) {
				_currentCollection.buildOut();
			}
			
			_currentCollection = null;
			_currentCollection = collection;
			_leftMostIndex = 0;
			createDestinationIndices( collection.getThumb( 0 ).width + THUMB_PADDING );
			
			var iteratorLength:int;
			
			if( _currentCollection.length <= _destinationIndices.length - 2  ) {
				iteratorLength = _currentCollection.length;
				disable();
			}
			else {
				iteratorLength = _destinationIndices.length - 2
				enable();
			}
			
			
			for( var i:int = 0; i < iteratorLength; i++ ) {
				
				var thumb:Thumb = _currentCollection.getThumb( i );
				thumb.x = _destinationIndices[ i + 1 ];
				_view.holder.addChild( thumb ); 
				thumb.buildIn();
				
			}
			
			if( _currentCollection.length <= 1 ) {
				_stepper.disable();
			}
			else {
				_stepper.enable();
			}
			
			_currentCollection.getThumb( 0 ).select();
			
		}
		
		
		
		public function scrollRight( e:MouseEvent = null ):void {
			
			_leftMostIndex--;
			if( _leftMostIndex < 0 ) {
				_leftMostIndex = _currentCollection.length - 1;
			}
			
			var scrollingThumbs:Array = getThumbsToScroll( _leftMostIndex ); 
			var offStageStartX:int = _destinationIndices[ 0 ]; 
			scrollAction( scrollingThumbs, 0, offStageStartX, 1 );
			
		}
		
		
		
		public function scrollLeft( e:MouseEvent = null ):void {
			
			var scrollingThumbs:Array = getThumbsToScroll( _leftMostIndex );
			var offStageStartX:int = _destinationIndices[  _destinationIndices.length - 1 ]; 
			scrollAction( scrollingThumbs, scrollingThumbs.length - 1, offStageStartX );
			
			_leftMostIndex++;
			if( _leftMostIndex == _currentCollection.length ) {
				_leftMostIndex = 0;
			}
			
		}
		
		
		
		private function scrollAction( thumbs:Array, offStageIndex:int, offStageStartX:int, targetIndexOffset:int = 0 ):void {
			
			for( var i:int = 0; i < thumbs.length; i++ ){
				
				var thumb:Thumb = thumbs[ i ];
				
				if( i == offStageIndex ) {
					thumb.x = offStageStartX;
					thumb.alpha = 1;
					if( !thumb.isSelected ){
						thumb.enable();
					}
					_view.holder.addChild( thumb );
				}
				
				Tweener.addTween( thumb, { x:_destinationIndices[ i + targetIndexOffset ], time:0.4	} );
				
			}
			
		}
		
		
		
		private function getThumbsToScroll( start:int ):Array {
			
			var thumbs:Array = new Array();
			
			for( var i:int = 0; i < _animThumbsAmt; i++ ) {
				
				var index:int = start + i;
				if( index >= _currentCollection.length ) {
					index -= _currentCollection.length;
				}
				thumbs.push( _currentCollection.getThumb( index ) );
				
			}
			
			return thumbs;
			
		}
		
		
		
		private function createDestinationIndices( incrementWidth:int ):void {
			
			_destinationIndices = null;
			_destinationIndices = [];
			
			var openSpots:int = ( _holderWidth / incrementWidth ) + 2;
			
			for( var i:int; i <= openSpots; i++ ) {
				_destinationIndices[ i ] = -incrementWidth + ( incrementWidth * i );
			}
			
			_animThumbsAmt = _destinationIndices.length - 1;
			
		}
		
		
		
		private function onStep( e:StepperEvent ):void {
			
			dispatchEvent( e );
			
		}
		

	}
}