package ca.budcamp.mediaSlideshow.thumbnails {
	
	import ca.budcamp.mediaSlideshow.events.StepperEvent;
	
	import caurina.transitions.Equations;
	import caurina.transitions.Tweener;
	
	import com.indusblue.ui.MovieClipButton;
	
	import flash.display.MovieClip;
	import flash.display.Stage;
	import flash.events.EventDispatcher;
	import flash.events.KeyboardEvent;
	import flash.events.MouseEvent;
	import flash.geom.Point;
	import flash.ui.Keyboard;
	
	[Event (name="step", type="ca.budcamp.mediaSlideshow.events.StepperEvent")]	
	
	/**
	 * 
	 * @author ghostmonk 09/05/2009
	 * 
	 */
	public class Stepper extends EventDispatcher {
		
		
		private var _next:MovieClipButton;
		private var _back:MovieClipButton;
		
		private var _nextEnabledPoint:Point;
		private var _nextDisabledPoint:Point;
		private var _backEnabledPoint:Point;
		private var _backDisabledPoint:Point;
		private var _stage:Stage;
		

		
		/**
		 * 
		 * @param nextBtn
		 * @param backBtn
		 * 
		 */
		public function Stepper( nextBtn:MovieClip, backBtn:MovieClip, stage:Stage ) {
			
			_nextEnabledPoint = new Point( nextBtn.x, nextBtn.y );
			_nextDisabledPoint = new Point( nextBtn.x + nextBtn.width * 0.5, nextBtn.y + nextBtn.height * 0.5 );
			_backEnabledPoint = new Point( backBtn.x, backBtn.y );
			_backDisabledPoint = new Point( backBtn.x + backBtn.width * 0.5, backBtn.y + backBtn.height * 0.5 );
			
			_next = new MovieClipButton( nextBtn, onNext );
			_back = new MovieClipButton( backBtn, onBack );	
			
			nextBtn.visible = backBtn.visible = false;
			nextBtn.alpha = backBtn.alpha = 0;
			
			_stage = stage;
			enableKeyEvent();
			
		}
		


		private function onKeydown( e:KeyboardEvent ):void {
			
			_stage.removeEventListener( KeyboardEvent.KEY_DOWN, onKeydown );
			_stage.addEventListener( KeyboardEvent.KEY_UP, enableKeyEvent );
			
			if( e.keyCode == Keyboard.LEFT ) {
				onBack();
			}
			
			if( e.keyCode == Keyboard.RIGHT) {
				onNext();				
			}
			
		}
		
		
		
		private function enableKeyEvent( e:KeyboardEvent = null ):void {
			
			_stage.addEventListener( KeyboardEvent.KEY_DOWN, onKeydown );
			
		}


		
		public function enable():void {
			
			buildIn( _next, _nextEnabledPoint );
			buildIn( _back, _backEnabledPoint );
			
		}
		
		
		
		public function disable():void {
			
			buildOut( _next, _nextDisabledPoint );
			buildOut( _back, _backDisabledPoint );
			
		}
		
		
		
		private function onNext( e:MouseEvent = null ):void {
			
			dispatchEvent( new StepperEvent( StepperEvent.STEP, true ) );
			
		}
		
		
		
		private function onBack( e:MouseEvent = null ):void {
			
			dispatchEvent( new StepperEvent( StepperEvent.STEP, false ) );
			
		}
		
		
		
		private function buildIn( button:MovieClipButton, openDest:Point ):void {
			
			button.enable();
			button.view.visible = true;
			
			Tweener.addTween( 
				button.view, {
					alpha:1,
					scaleX:1, 
					scaleY:1, 
					x:openDest.x, 
					y:openDest.y, 
					time:0.3,
					transition:Equations.easeOutBack	
				}
			);
			
		}
		
		
		
		private function buildOut( button:MovieClipButton, closeDest:Point ):void {
			
			button.disable();
			
			Tweener.addTween( 
				button.view, {
					alpha:0,
					scaleX:0, 
					scaleY:0, 
					x:closeDest.x, 
					y:closeDest.y, 
					time:0.3,
					transition:Equations.easeInBack	
				}
			);
			
		}
		
		

	}
}