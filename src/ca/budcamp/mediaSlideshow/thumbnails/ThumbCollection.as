package ca.budcamp.mediaSlideshow.thumbnails {
	
	import ca.budcamp.mediaSlideshow.events.ThumbClickEvent;
	
	import flash.events.EventDispatcher;
	
	[Event (name="thumbClick", type="ca.budcamp.mediaSlideshow.events.ThumbClickEvent")]
	
	/**
	 * 
	 * @author ghostmonk 09/05/2009
	 * 
	 */
	public class ThumbCollection extends EventDispatcher {
		
		
		
		private var _thumbs:Array;
		
		
		
		public function ThumbCollection() {
			
			_thumbs = new Array();
			
		}
		
		
		
		public function push( thumb:Thumb ):void {
			
			_thumbs.push( thumb );
			thumb.index = _thumbs.indexOf( thumb );
			thumb.addEventListener( ThumbClickEvent.THUMB_CLICK, onThumbClick );
			
		}
		
		
		
		public function getThumb( index:int ):Thumb {
			
			return _thumbs[ index ] as Thumb;
			
		}
		
		
		
		public function get length():int {
			
			return _thumbs.length;
			
		}
		
		
		
		public function buildIn():void {
			
			for each( var thumb:Thumb in _thumbs ) {
				thumb.buildIn();
			}
			
		}
		
		
		
		public function buildOut():void {
			
			for each( var thumb:Thumb in _thumbs ) {
				thumb.buildOut();
			}
			
		}
		
		
		
		private function onThumbClick( e:ThumbClickEvent ):void {
			
			for each( var thumb:Thumb in _thumbs ) {
				if( ( e.target as Thumb ) == thumb) {
					thumb.select();
				}
				else {
					thumb.deselect();
				}
			}
			
			dispatchEvent( e );
			
		}



	}
}