package ca.budcamp.mediaSlideshow.video {
	
	import ca.budcamp.mediaSlideshow.events.ThumbClickEvent;
	import ca.budcamp.mediaSlideshow.section.BaseSection;
	
	import caurina.transitions.Tweener;
	
	import com.indusblue.events.PercentageEvent;
	import com.indusblue.media.video.CoreVideo;
	import com.indusblue.media.video.events.MetaInfoEvent;
	import com.indusblue.media.video.events.VideoControlEvent;
	
	import flash.display.DisplayObjectContainer;
	import flash.events.Event;
	/**
	 * 
	 * @author ghostmonk 09/05/2009
	 * 
	 */
	public class VideoPlayer extends BaseSection {
		
		
		
		private var _coreVideo:CoreVideo;
		private var _controlPanel:VideoControlPanel;
		private var _container:DisplayObjectContainer;
		private var _openPanelY:Number;
		private var _closedPanelY:Number;
		
		
		
		/**
		 * 
		 * @param id
		 * @param coreVideo
		 * @param controls
		 * @param container
		 * @param startYear
		 * 
		 */
		public function VideoPlayer( id:String, coreVideo:CoreVideo, controls:VideoControlPanel, container:DisplayObjectContainer, startYear:String ) {
			
			super( id, coreVideo, startYear );
			
			_coreVideo = coreVideo;
			_coreVideo.addEventListener( PercentageEvent.LOAD_CHANGE, onPercentageChange );
			_coreVideo.addEventListener( MetaInfoEvent.META_INFO_READY, onMetaInfoReady );
			
			_controlPanel = controls;
			_controlPanel.addEventListener( PercentageEvent.CHANGE, onScrub );
			_controlPanel.addEventListener( VideoControlEvent.PAUSE, onPause );
			_controlPanel.addEventListener( VideoControlEvent.PLAY, onPlay );
			
			_openPanelY = _controlPanel.view.y;
			_closedPanelY = _controlPanel.view.y += _controlPanel.view.height;
			
			_container = container;
			
		}
		0
		
		
		override public function buildIn():void {
			
			super.buildIn();
			
			playVideo( thumbs.getThumb( 0 ).fullAsset );
			_controlPanel.pause();
			Tweener.addTween(_controlPanel.view, { y:_openPanelY, time:0.3 } );
			
		}
		
		
		
		override public function buildOut():void {
			
			super.buildOut();
			Tweener.addTween(_controlPanel.view, { y:_closedPanelY, time:0.3 } );
			_coreVideo.close();
			removeEventListener( Event.ENTER_FRAME, onEnterFrame );
			
		}
		
		
		
		override protected function onThumbClick( e:ThumbClickEvent ):void {
			
			currentIndex = e.index;
			playVideo( e.id );
			_controlPanel.pause();
			
		}
		
		
		
		private function playVideo( url:String ):void {
			
			_coreVideo.load( url, true, true );
			
		}
		
		
		
		private function onPercentageChange( e:PercentageEvent ):void {
			
			_controlPanel.loadPercent = e.percent ;
			
		}
		
		
		
		private function onMetaInfoReady( e:MetaInfoEvent ):void {
			
			_coreVideo.scaleAndCenterToContainer( _container );
			addEventListener( Event.ENTER_FRAME, onEnterFrame );
			
		}
		
		
		
		private function onEnterFrame( e:Event ):void {
			
			_controlPanel.timeAsPercent = _coreVideo.currentTimeAsPercent;
			
		}
		
		
		
		private function onScrub( e:PercentageEvent ):void {
			
			_coreVideo.currentTimeAsPercent = e.percent;
			 
		}
		
		
		
		private function onPlay( e:VideoControlEvent ):void {
			
			_coreVideo.play();
			_controlPanel.play();
			
		}
		
		
		
		private function onPause( e:VideoControlEvent ):void {
			
			_coreVideo.pause();
			_controlPanel.pause();
			
		}
		
		
		
	}
}