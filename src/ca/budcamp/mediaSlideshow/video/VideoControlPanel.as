package ca.budcamp.mediaSlideshow.video {
	
	import com.indusblue.events.PercentageEvent;
	import com.indusblue.media.video.events.VideoControlEvent;
	import com.indusblue.media.video.ui.PlayPauseToggle;
	import com.indusblue.media.video.ui.SimpleVideoScrubber;
	
	import flash.events.Event;
	import flash.events.EventDispatcher;
	
	[Event(name="change", type="com.indusblue.events.PercentageEvent")]
	[Event(name="play", type="com.indusblue.media.video.events.VideoControlEvent")]
	[Event(name="pause", type="com.indusblue.media.video.events.VideoControlEvent")]
	
	/**
	 * 
	 * @author ghostmonk 09/05/2009
	 * 
	 */
	public class VideoControlPanel extends EventDispatcher {
		
		
		
		private var _view:ControlPanelAsset;
		private var _playToggle:PlayPauseToggle;
		private var _scrubber:SimpleVideoScrubber;
		
		
		
		public function get view():ControlPanelAsset {
			
			return _view;
			
		}
		
		
		
		/**
		 * 
		 * @param view
		 * @param playToggle
		 * @param scrubber
		 * 
		 */
		public function VideoControlPanel( view:ControlPanelAsset, playToggle:PlayPauseToggle, scrubber:SimpleVideoScrubber ) {
			
			_view = view;
			_playToggle = playToggle;
			_scrubber = scrubber;
			
			_playToggle.addEventListener( VideoControlEvent.PAUSE, onControl );
			_playToggle.addEventListener( VideoControlEvent.PLAY, onControl );
			
			_scrubber.addEventListener( PercentageEvent.CHANGE, onControl );
			_scrubber.addEventListener( VideoControlEvent.PAUSE, onControl );
			_scrubber.addEventListener( VideoControlEvent.PLAY, onControl );
			
		}
		
		
		
		/**
		 * 
		 * @param percent
		 * 
		 */
		public function set loadPercent( percent:Number ):void {
			
			_scrubber.onLoad( percent );
			
		}
		
		
		
		/**
		 * 
		 * @param timeAsPercent
		 * 
		 */
		public function set timeAsPercent( timeAsPercent:Number ):void {
			
			_scrubber.setScrubber( timeAsPercent );
			
		}
		
		
		
		public function pause():void {
			
			_playToggle.play = false;
			
		}
		
		
		public function play():void {
			
			_playToggle.play = true;
			
		}
		
		
		private function onControl( e:Event ):void {
			
			dispatchEvent( e );
			
		}
		
		

	}
}