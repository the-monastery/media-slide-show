package ca.budcamp.mediaSlideshow.navigation {
	
	import ca.budcamp.mediaSlideshow.events.YearChoiceEvent;
	
	import caurina.transitions.Equations;
	import caurina.transitions.Tweener;
	
	import com.indusblue.ui.MovieClipButton;
	
	import flash.display.DisplayObject;
	import flash.display.MovieClip;
	import flash.events.EventDispatcher;
	import flash.events.MouseEvent;
	
	
	[Event (name="yearChoice", type="ca.budcamp.mediaSlideshow.events.YearChoiceEvent")]
	
	/**
	 * 
	 * @author ghostmonk 09/05/2009
	 * 
	 */
	public class YearChooser extends EventDispatcher {
		
		
		
		private var _watermark:WaterMark;
		private var _yearButtons:Object; 
		private var _currentYear:String;
		private var _yearIDs:Array;
		private var _positionMarker:DisplayObject;
		
		
		
		/**
		 * 
		 * @param watermark
		 * @param positionMarker
		 * 
		 */
		public function YearChooser( watermark:WaterMark, positionMarker:DisplayObject ) {
			
			_watermark = watermark; 
			_positionMarker = positionMarker;
			
			_yearButtons = new Object();
			_yearIDs = new Array();
			
		}
		
		
		
		/**
		 * 
		 * @param id
		 * @param button
		 * 
		 */
		public function addChooser( id:String, button:MovieClip ):void {
			
			button.id = id;
			_yearButtons[ id ] = new MovieClipButton( button, onYearChoice );
			button.visible = true;
			_yearIDs.push( id );
		}
		
		
		
		/**
		 * When setting the current year, the target MovieClipButton will disable
		 * and the watermark will update.
		 * 
		 * @return 
		 * 
		 */
		public function get currentYear():String {
			
			return _currentYear;
			
		}
		
		
		
		public function set currentYear( id:String ):void {
			
			_currentYear = id;
			_watermark.label.text = id;
			
			for each( var button:MovieClipButton in _yearButtons ) {
				
				if( button.view.id == id ) {
					button.disable( true );
				}
				else {
					button.enable();
				}
			}
			
		}
		
		
		
		/**
		 * 
		 * @param idList
		 * @param currentID
		 * 
		 */
		public function configureButtons( idList:Array, currentID:String ):void {
			
			_currentYear = currentID;
			_watermark.label.text = _currentYear;
			
			var yearButtons:Array = new Array();
			
			for each( var id:String in idList ) {
				yearButtons.push( _yearButtons[ id ] );
			}
			
			if( idList.length < 2 ) {	
				removeAll();
			}
			else {
				filterDisplay( yearButtons );
			}
			
			positionButtons( yearButtons );
			( _yearButtons[ _currentYear ] as MovieClipButton ).disable( true );
		}
		
		
		
		private function positionButtons( choosers:Array ):void {
			
			for( var i:int = 0; i < choosers.length; i++ ) {
				
				var button:MovieClipButton = choosers[ i ];
				button.view.x = ( button.view.width + 10 ) * i + _positionMarker.x;
				 
			}
			
		}
		
		
		
		private function removeAll():void {
			
			for each( var button:MovieClipButton in _yearButtons ) {
				buildOut( button );
			}
			
		}
		
		
		
		private function filterDisplay( yearButtons:Array ):void {
			
			for each( var button:MovieClipButton in _yearButtons ) {
				if( yearButtons.indexOf( button ) == -1 ) {
					buildOut( button );
				}
				else {
					buildIn( button );
				}
			}
				
		}
		
		
		
		private function onYearChoice( e:MouseEvent ):void {
			
			currentYear = ( e.target as MovieClip ).id;
			dispatchEvent( new YearChoiceEvent( YearChoiceEvent.YEAR_CHOICE, _watermark.label.text ) );
				
		}
		
		
		
		private function buildOut( button:MovieClipButton ):void {
		
			button.view.visible = false;
			button.disable( true, false );
			button.view.alpha = 0;
			
		}
		
		
		
		private function buildIn( button:MovieClipButton ):void {
			
			button.view.visible = true;
			button.view.alpha = 0;
			button.enable( false );
			Tweener.addTween( button.view, { alpha:1, time:0.4, transition:Equations.easeNone } );
			
		}
		
		
		
	}
}