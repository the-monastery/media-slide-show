package ca.budcamp.mediaSlideshow.navigation {
	
	import ca.budcamp.mediaSlideshow.events.SectionNavigationEvent;
	
	import com.indusblue.ui.MovieClipButton;
	
	import flash.display.MovieClip;
	import flash.events.EventDispatcher;
	import flash.events.MouseEvent;
	import flash.events.TimerEvent;
	import flash.utils.Timer;
	
	[Event (name="sectionNavigation", type="ca.budcamp.mediaSlideshow.events.SectionNavigationEvent")]
	
	/**
	 * 
	 * @author ghostmonk 09/05/2009
	 * 
	 */
	public class TabNavigator extends EventDispatcher {
		
		
		
		private var _timer:Timer;
		private var _links:Array;
		private var _sectionID:String;
		
		
		
		public function TabNavigator() {
			
			_links = new Array();
			_timer = new Timer( 500, 1 );
			_timer.addEventListener( TimerEvent.TIMER_COMPLETE, onTimerComplete );
			
		}
		
		
		
		public function addLink( button:MovieClip, id:String ):void {
			
			button.id = id;
			var link:MovieClipButton = new MovieClipButton( button, onClick );
			_links.push( link ); 
			
		}
		
		
		
		public function setLink( id:String ):void {
			
			for each( var link:MovieClipButton in _links ) {
				if( link.view.id == id ) {
					link.disable( true );
				}
			}
			
		}
		
		
		
		private function onClick( e:MouseEvent ):void {
			
			_timer.start();
			var sectionID:String = ( e.target as MovieClip ).id; 
			_sectionID = sectionID;
			
			dispatchEvent( new SectionNavigationEvent( 
				SectionNavigationEvent.SECTION_NAVIGATION, 
				_sectionID ) 
			);
			
			for each( var link:MovieClipButton in _links ){
				if(  _sectionID != link.view.id ) {
					link.disable();
				}
				else {
					link.disable( true );
				}
			}
				
		}
		
		
		private function onTimerComplete( e:TimerEvent ):void {
			
			_timer.stop();
			_timer.reset();
			
			for each( var link:MovieClipButton in _links ){
				if( _sectionID != link.view.id ) {
					link.enable();
				}
			}
			
			
		}



	}
}