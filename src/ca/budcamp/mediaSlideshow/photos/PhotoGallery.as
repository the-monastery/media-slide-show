package ca.budcamp.mediaSlideshow.photos {
	
	import ca.budcamp.mediaSlideshow.events.ThumbClickEvent;
	import ca.budcamp.mediaSlideshow.section.BaseSection;
	import ca.budcamp.mediaSlideshow.thumbnails.Thumb;
	
	
	/**
	 * 
	 * @author ghostmonk 09/05/2009
	 * 
	 */
	public class PhotoGallery extends BaseSection {
		
		
		
		private var _galleries:Object;
		private var _currentGallery:Array;
		private var _currentImage:GalleryImage;
		
		
		public function PhotoGallery( id:String, startYear:String ) {
			
			super( id, this, startYear );
			_galleries = new Object();
			
		}
		
		
		
		override public function buildIn():void {
			
			super.buildIn();
			
			if( _galleries[ currentYear ] == null ) {
				
				var gallery:Array = new Array(); 
			
				for( var i:int = 0; i < thumbs.length; i++ ) {
					var thumb:Thumb = thumbs.getThumb( i );
					gallery.push( new GalleryImage( thumb.fullAsset, new ImageLoaderAsset(), 835, 469 ) );
				}
				
				_galleries[ currentYear ] = gallery;
					
			}
			
			if( _currentImage ) {
				_currentImage.buildOut();
			}
			
			_currentGallery = _galleries[ currentYear ]; 
			_currentImage = _currentGallery[ 0 ];
			
			addChild( _currentImage );
			_currentImage.buildIn();
			
		}
		
		
		override protected function onThumbClick( e:ThumbClickEvent ):void {
			
			currentIndex = e.index;
			_currentImage.buildOut();
			
			for each( var galleryImage:GalleryImage in _currentGallery ) {
				if( galleryImage.id == e.id ) {
					_currentImage = galleryImage;
					break;
				}
			}
			
			addChild( _currentImage );
			_currentImage.buildIn();
			
		}
		
		
		
	}
}