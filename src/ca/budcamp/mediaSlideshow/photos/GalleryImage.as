package ca.budcamp.mediaSlideshow.photos {
	
	import caurina.transitions.Equations;
	import caurina.transitions.Tweener;
	
	import com.indusblue.net.AssetLoader;
	
	import flash.display.Bitmap;
	import flash.display.MovieClip;
	import flash.display.Sprite;

	/**
	 * 
	 * @author ghostmonk 09/05/2009
	 * 
	 */
	public class GalleryImage extends Sprite {
		
		
		
		private var _bitmap:Bitmap;
		private var _id:String;
		private var _loader:MovieClip;
		
		
		
		public function get id():String {
			
			return _id;
			
		}
		
		
		public function GalleryImage( url:String, progressMeter:MovieClip, parentWidth:Number, parentHeight:Number ) {
			
			alpha = 0;
			_id = url;
			_loader = progressMeter;
			_loader.x = ( parentWidth - _loader.width ) * 0.5;
			_loader.y = ( parentHeight - _loader.height ) * 0.5;
			addChild( _loader );
			new AssetLoader( url, onImageLoaded, progressMeter );
			
		}
		
		
		
		public function buildIn():void {
			
			Tweener.addTween( this, { alpha:1, time:0.3, transition:Equations.easeNone } );
			
		}
		
		
		
		public function buildOut():void {
			
			Tweener.addTween( this, { alpha:0, time:0.3, transition:Equations.easeNone, onComplete:removeThis } );
			
		}
		
		
		
		private function removeThis():void {
			
			if( this.parent ) {
				parent.removeChild( this );
			}
			
		}
		
		
		private function onImageLoaded( data:Bitmap ):void {
			
			removeChild( _loader );
			_loader = null;
			_bitmap = data;
			_bitmap.alpha = 0;
			addChild( _bitmap );
			Tweener.addTween( _bitmap, { alpha:1, time:0.3, transition:Equations.easeNone } );
			
		}
		
		
		
	}
}