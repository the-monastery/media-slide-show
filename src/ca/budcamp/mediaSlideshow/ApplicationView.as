package ca.budcamp.mediaSlideshow {
	
	import caurina.transitions.Equations;
	import caurina.transitions.Tweener;
	
	import flash.display.DisplayObject;
	import flash.events.Event;
	
	[Event (name="complete", type="flash.events.Event")]
	
	/**
	 * 
	 * @author ghostmonk 09/05/2009
	 * 
	 */
	public class ApplicationView extends ApplicationAssetLayout {
		
		
		
		public function ApplicationView( videosID:String, photosID:String ) {
			
			videosTab.label.field.text = videosID;
			photosTab.label.field.text = photosID;
			sectionTitle.label.text = videosID;
			background.mouseEnabled = false;
			background.mouseChildren = false;	
			
			addEventListener( Event.ADDED_TO_STAGE, buildInView );
			
		}
		
		
		
		private function buildInView( e:Event ):void {
		
			background.visible =false;
			photosTab.visible = false;
			videosTab.visible = false;
			logo.visible = false;
			sectionTitle.visible = false;
			watermark.visible = false;
			thumbCarousel.visible = false;
			viewContainer.visible = false;
			
			bounceAnim( background, background.x, background.y);
			bounceAnim( logo, logo.x, logo.y, 0.3 );
			bounceAnim( viewContainer, viewContainer.x, viewContainer.y, 0.3 );
			bounceAnim( sectionTitle, sectionTitle.x, sectionTitle.y, 0.3 );
			slideUp( photosTab, photosTab.y, 0.4 );
			slideUp( videosTab, videosTab.y, 0.3 );
			slideUp( watermark.label, watermark.label.y, 0.4 );
			watermark.visible = true;
			animateCarousel( thumbCarousel.x, 0.4 );
			
		}
		
		
		
		private function bounceAnim( display:DisplayObject, xDest:Number, yDest:Number, delay:Number = 0 ):void {
			
			display.x += display.width * 0.5;
			display.y += display.height * 0.5;
			display.scaleX = display.scaleY = 0;
			
			Tweener.addTween( 
				display, { 
					scaleX:1, 
					scaleY:1, 
					x:xDest, 
					y:yDest,
					delay:delay,
					onStart:show,
					onStartParams:[display], 
					time:0.3,
					transition:Equations.easeOutBack
				}
			);
			
		}
		
		
		
		private function slideUp( display:DisplayObject, yDest:Number, delay:Number ):void {
			
			display.y += display.height;
			
			Tweener.addTween( 
				display, { 
					y:yDest, 
					delay:delay, 
					time:0.3, 
					onStart:show,
					onStartParams:[display]
				} 
			);
			
		}
		
		
		
		private function animateCarousel( xDest:Number, delay:Number ):void {
			
			thumbCarousel.x += 500;
			thumbCarousel.alpha = 0;
			
			Tweener.addTween( 
				thumbCarousel, {
					x:xDest, 
					delay:delay, 
					time:0.3, 
					onStart:show,
					onStartParams:[thumbCarousel],
					onComplete:dispatchComplete
				} 
			);
			
			Tweener.addTween( thumbCarousel, { alpha:1, time:0.5, transition:Equations.easeNone } );
			
		}
		
		
		
		private function show( display:DisplayObject ):void {
			
			display.visible = true;
			
		}
		
		
		
		private function dispatchComplete():void {
			
			dispatchEvent( new Event( Event.COMPLETE ) );
			
		}
		
		
		
	}
}