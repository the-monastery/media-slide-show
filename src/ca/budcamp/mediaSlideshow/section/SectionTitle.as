package ca.budcamp.mediaSlideshow.section {
	import caurina.transitions.Equations;
	import caurina.transitions.Tweener;
	import caurina.transitions.properties.TextShortcuts;
	
	
	/**
	 * 
	 * @author nhillier
	 * 
	 */
	public class SectionTitle {
		
		
		
		private var _view:SectionTitleAsset;
		
		
		
		/**
		 * 
		 * @return 
		 * 
		 */
		public function get view():SectionTitleAsset {
			
			return _view;
			
		}
		
		
		
		/**
		 * 
		 * @param view
		 * 
		 */
		public function SectionTitle( view:SectionTitleAsset, startSection:String ) {
			
			TextShortcuts.init();
			_view = view;
			_view.label.text = startSection; 
						
		}


		/**
		 * 
		 * @param value
		 * 
		 */
		public function set title( value:String ):void {
			
			Tweener.addTween( 
				_view.label, { 
					_text:value,
					time:0.3,
					transition:Equations.easeNone
				}
			);
			
		} 


	}
}