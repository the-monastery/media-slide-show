package ca.budcamp.mediaSlideshow.section {
	import ca.budcamp.mediaSlideshow.thumbnails.ThumbCollection;
	
	import flash.display.DisplayObjectContainer;
	
	
	/**
	 * 
	 * @author ghostmonk 09/05/2009
	 * 
	 */
	public interface ISection {
		
		
		
		/**
		 * 
		 * @param value
		 * 
		 */
		function set currentYear( value:String ):void;
		
		
		
		/**
		 * 
		 * @param value
		 * 
		 */
		function get currentYear():String;
		
		
		
		/**
		 * 
		 * @param value
		 * 
		 */
		function get yearIDs():Array;
		
		
		
		/**
		 * 
		 * @return 
		 * 
		 */
		function get display():DisplayObjectContainer;
		
		
		/**
		 * 
		 * @return 
		 * 
		 */
		function addCollection( id:String, collection:ThumbCollection ):void;
		
		
		/**
		 * 
		 * @return 
		 * 
		 */
		function get thumbs():ThumbCollection;
		
		
		
		/**
		 * 
		 * @return 
		 * 
		 */
		function get id():String;
		
		
		
		/**
		 * 
		 * 
		 */
		function buildIn():void;
		
		
		
		/**
		 * 
		 * 
		 */
		function buildOut():void;
		
	}
}