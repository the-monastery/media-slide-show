package ca.budcamp.mediaSlideshow.section {
	
	import ca.budcamp.mediaSlideshow.events.ThumbClickEvent;
	import ca.budcamp.mediaSlideshow.thumbnails.Thumb;
	import ca.budcamp.mediaSlideshow.thumbnails.ThumbCollection;
	
	import caurina.transitions.Equations;
	import caurina.transitions.Tweener;
	
	import flash.display.DisplayObjectContainer;
	import flash.display.Sprite;
	
	[Event (name="thumbClick", type="ca.budcamp.mediaSlideshow.events.ThumbClickEvent")]
	
	public class BaseSection extends Sprite implements ISection {
		
		
		
		private var _id:String;
		private var _currentYear:String;
		private var _display:DisplayObjectContainer;
		private var _yearIDs:Array;
		private var _currentIndex:int;
		protected var _thumbs:Object;
		
		
		public function get currentIndex():int {
			return _currentIndex;
		}
		
		
		
		public function set currentIndex( value:int ):void {
			_currentIndex = value;
		}
		
		
		
		public function BaseSection( id:String, display:DisplayObjectContainer, startYear:String ) {
			
			_id = id;
			_display = display;
			_thumbs = new Object();
			_currentYear = startYear; 
			_yearIDs = new Array();
			
			_display.alpha = 0;
			
		}
		
		
		
		public function addCollection( id:String, collection:ThumbCollection ):void {
			
			collection.addEventListener( ThumbClickEvent.THUMB_CLICK, onThumbClick );
			_thumbs[ id ] = collection;
			_yearIDs.push( id );
			
		}
		
		
		
		public function set currentYear( value:String ):void {
			
			_currentYear = value;
			
		}
		
		
		
		public function get currentYear():String {
			
			if( _yearIDs.indexOf( _currentYear ) == -1 ) {
				_currentYear = _yearIDs[ 0 ];
			}
			return _currentYear;
			
		}
		
		
		
		public function get yearIDs():Array {
			
			return _yearIDs;
			
		}
		
		
		
		public function get id():String {
			
			return _id;
			
		}



		public function get display():DisplayObjectContainer {
			
			return _display;
			
		}
		
		
		
		public function get thumbs():ThumbCollection {
			
			//Make sure the start year was not passed in incorrectly
			//There should always be at least one year choice
			if( _yearIDs.indexOf( _currentYear ) == -1 ) {
				_currentYear = _yearIDs[ 0 ];
			}
			
			return _thumbs[ _currentYear ] as ThumbCollection;
			
		}
		
		
		
		public function buildIn():void {
			
			_display.alpha = 0;
			_currentIndex = 0;
			Tweener.removeTweens( _display );
			Tweener.addTween( _display, { alpha:1, time:0.3, transition:Equations.easeNone } );
			
		}
		
		
		
		public function buildOut():void {
			
			Tweener.removeTweens( _display );
			Tweener.addTween( _display, { alpha:0, time:0.3, transition:Equations.easeNone, onComplete:removeThis} );
			
		}
		
		
		private function removeThis():void {
			if( _display.parent ) {
				_display.parent.removeChild( _display );
			}
		}
		
		
		protected function onThumbClick( e:ThumbClickEvent ):void {
			
		}
		
		
		
		public function incrementAsset( isNext:Boolean ):void {
			
			var increment:int = isNext ? 1 : -1;
			_currentIndex += increment;
			
			if( _currentIndex < 0 ) {
				_currentIndex = thumbs.length - 1;
			}
			
			if( _currentIndex == thumbs.length ) {
				_currentIndex = 0;
			}
			
			var nextThumb:Thumb = thumbs.getThumb( _currentIndex );
			
			nextThumb.dispatchEvent( new ThumbClickEvent( ThumbClickEvent.THUMB_CLICK, nextThumb.fullAsset, nextThumb.index ) ); 
			
		}
		
		
		
	}
}