package ca.budcamp.mediaSlideshow.section {
	
	import ca.budcamp.mediaSlideshow.events.SectionNavigationEvent;
	import ca.budcamp.mediaSlideshow.events.StepperEvent;
	import ca.budcamp.mediaSlideshow.events.YearChoiceEvent;
	import ca.budcamp.mediaSlideshow.navigation.TabNavigator;
	import ca.budcamp.mediaSlideshow.navigation.YearChooser;
	import ca.budcamp.mediaSlideshow.thumbnails.ThumbCarousel;
	
	import flash.display.MovieClip;
	
	
	/**
	 * 
	 * @author ghostmonk 09/05/2009
	 * 
	 */
	public class SectionManager {
		
		
		
		private var _sections:Array;
		private var _tabNavigator:TabNavigator;
		private var _thumbCarousel:ThumbCarousel;
		private var _viewContainer:ViewContainer;
		private var _sectionTitle:SectionTitle;
		private var _yearChooser:YearChooser;
		
		private var _currentSection:BaseSection;
		
		
		
		/**
		 * 
		 * @param tabNavigator
		 * @param thumbCarousel
		 * @param view
		 * @param title
		 * @param yearChooser
		 * 
		 */
		public function SectionManager( 
				tabNavigator:TabNavigator, 
				thumbCarousel:ThumbCarousel, 
				view:ViewContainer, 
				title:SectionTitle, 
				yearChooser:YearChooser ) {
			
			_sections = new Array();
			
			_tabNavigator = tabNavigator;
			tabNavigator.addEventListener( SectionNavigationEvent.SECTION_NAVIGATION, onNavigation );
			
			_thumbCarousel = thumbCarousel;
			_thumbCarousel.addEventListener( StepperEvent.STEP, onStep );
			
			_viewContainer = view;
			_sectionTitle = title;
			_yearChooser = yearChooser;
			_yearChooser.addEventListener( YearChoiceEvent.YEAR_CHOICE, onYearChoice );
			
		}
		
		
		
		/**
		 * 
		 * @param section
		 * @param link
		 * 
		 */
		public function addSection( section:BaseSection, link:MovieClip ):void {
			
			_sections.push( section );
			_tabNavigator.addLink( link, section.id );
			
		}
		
		
		
		/**
		 * 
		 * @param startSectionID
		 * 
		 */
		public function init( startSectionID:String ):void {
			
			setCurrentSection( startSectionID );
			_thumbCarousel.addThumbs( _currentSection.thumbs );
			
		}
		
		
		
		private function onNavigation( e:SectionNavigationEvent ):void {
			
			setCurrentSection( e.id );
			
		}
		
		
		
		private function setCurrentSection( sectionID:String ):void {
			
			for each( var section:BaseSection in _sections ) {
				if( section.id == sectionID ) {
					_currentSection = section;
				}
			}
			
			_sectionTitle.title = sectionID;
			_tabNavigator.setLink( sectionID );
			_thumbCarousel.addThumbs( _currentSection.thumbs );
			_viewContainer.section = _currentSection;
			_yearChooser.configureButtons( _currentSection.yearIDs, _currentSection.currentYear );
			
		}
		
		
		
		private function onYearChoice( e:YearChoiceEvent ):void {
			
			_currentSection.currentYear = e.id;
			_currentSection.buildIn();
			_thumbCarousel.addThumbs( _currentSection.thumbs );
			
		}
		
		
		
		private function onStep( e:StepperEvent ):void {
			
			if( _thumbCarousel.isScrollable ) {
				checkThumbPosition( e.isNext );
			}
			
			_currentSection.incrementAsset( e.isNext );
			
		}
		
		
		
		private function checkThumbPosition( isNext:Boolean ):void {
			
			var visibleThumbPosition:int = _currentSection.currentIndex - _thumbCarousel.leftMostIndex;
			
			trace( visibleThumbPosition );
			
			var isOffRightNormal:Boolean = 
				visibleThumbPosition == ( _thumbCarousel.visibleSpaces - 1 ) 
				&& isNext;
				
			var isOffRightEndCase:Boolean = 
				( _thumbCarousel.visibleSpaces - 1 ) - _currentSection.thumbs.length == visibleThumbPosition 
				&& isNext;
				
			var isOffLeft:Boolean = visibleThumbPosition == 0 && !isNext;
			
			if( isOffLeft ) {
				_thumbCarousel.scrollRight();
			}
			else if( isOffRightEndCase || isOffRightNormal ) {
				_thumbCarousel.scrollLeft();
			}
			
		}
		
		
		
	}
}