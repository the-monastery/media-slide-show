package ca.budcamp.mediaSlideshow.section {
	
	import flash.display.DisplayObjectContainer;
	import flash.display.MovieClip;
	
	
	/**
	 * 
	 * @author ghostmonk 09/05/2009
	 * 
	 */
	public class ViewContainer {
		
		private var _view:ViewContainerAsset;
		private var _currentDisplay:ISection;
		
		
		public function get view():DisplayObjectContainer {
			
			return _view.holder;
			
		}
		
		
		
		public function get controlPanel():ControlPanelAsset {
			
			return _view.controlPanel;
			
		}
		
		
		
		public function get playBtn():MovieClip {
			
			return _view.controlPanel.playBtn;
			
		}
		
		
		public function get pauseBtn():MovieClip {
			
			return _view.controlPanel.pauseBtn;
			
		}
		
		public function get scrubAsset():ScrubberAsset {

			return  _view.controlPanel.scrubber;
			
		}
		
		public function ViewContainer( view:ViewContainerAsset ) {
			
			_view = view;
			
		}
		
		public function set section( display:ISection ):void {
			
			if( _currentDisplay ) {
				_currentDisplay.buildOut();
			}
			_currentDisplay = display;
			_view.holder.addChild( _currentDisplay.display );
			_currentDisplay.buildIn();
			
		}

	}
}